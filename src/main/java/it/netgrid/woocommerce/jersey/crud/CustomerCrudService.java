package it.netgrid.woocommerce.jersey.crud;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.Customer;
import it.netgrid.woocommerce.model.request.CustomerRequest;
import it.netgrid.woocommerce.model.response.CustomerResponse;

public class CustomerCrudService extends TemplateCrudService<Customer, Integer, Object, CustomerRequest, CustomerResponse> {

	public static final String BASE_PATH = "customers";
	public static final String BASE_PATH_FORMAT = "customers/%d";
	
	public CustomerCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Object context) {
		return BASE_PATH;
	}

	@Override
	public String getReadPath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getUpdatePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getDeletePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public Class<CustomerResponse> getResponseClass() {
		return CustomerResponse.class;
	}

	@Override
	public CustomerRequest getRequest(Customer object) {
		return new CustomerRequest(object);
	}

	@Override
	public Customer getResult(CustomerResponse network) {
		return network.getCustomer();
	}

}
