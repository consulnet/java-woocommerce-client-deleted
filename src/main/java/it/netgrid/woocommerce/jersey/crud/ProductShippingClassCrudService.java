package it.netgrid.woocommerce.jersey.crud;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.ProductShippingClass;
import it.netgrid.woocommerce.model.request.ProductShippingClassRequest;
import it.netgrid.woocommerce.model.response.ProductShippingClassResponse;

public class ProductShippingClassCrudService extends TemplateCrudService<ProductShippingClass, Integer, Object, ProductShippingClassRequest, ProductShippingClassResponse> {

	public static final String BASE_PATH = "products/shipping_classes";
	public static final String BASE_PATH_FORMAT = "products/shipping_classes/%d";	
	
	public ProductShippingClassCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Object context) {
		return BASE_PATH;
	}

	@Override
	public String getReadPath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getUpdatePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getDeletePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public Class<ProductShippingClassResponse> getResponseClass() {
		return ProductShippingClassResponse.class;
	}

	@Override
	public ProductShippingClassRequest getRequest(ProductShippingClass object) {
		return new ProductShippingClassRequest(object);
	}

	@Override
	public ProductShippingClass getResult(ProductShippingClassResponse network) {
		return network.getProductShippingClass();
	}

}
