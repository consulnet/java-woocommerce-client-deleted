package it.netgrid.woocommerce.jersey.crud;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.ProductTag;
import it.netgrid.woocommerce.model.request.ProductTagRequest;
import it.netgrid.woocommerce.model.response.ProductTagResponse;

public class ProductTagCrudService extends TemplateCrudService<ProductTag, Integer, Object, ProductTagRequest, ProductTagResponse> {
	
	public static final String BASE_PATH = "products/tags";
	public static final String BASE_PATH_FORMAT = "products/tags/%d";
	
	public ProductTagCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Object context) {
		return BASE_PATH;
	}

	@Override
	public String getReadPath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getUpdatePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getDeletePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public Class<ProductTagResponse> getResponseClass() {
		return ProductTagResponse.class;
	}

	@Override
	public ProductTagRequest getRequest(ProductTag object) {
		return new ProductTagRequest(object);
	}

	@Override
	public ProductTag getResult(ProductTagResponse network) {
		return network.getProductTag();
	}

}
