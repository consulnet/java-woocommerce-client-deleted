package it.netgrid.woocommerce.jersey.crud;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.Coupon;
import it.netgrid.woocommerce.model.request.CouponRequest;
import it.netgrid.woocommerce.model.response.CouponResponse;

public class CouponCrudService extends TemplateCrudService<Coupon, Integer, Object, CouponRequest, CouponResponse> {

	public static final String BASE_PATH = "coupons";
	public static final String BASE_PATH_FORMAT = "coupons/%d";

	public CouponCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Object context) {
		return BASE_PATH;
	}

	@Override
	public String getReadPath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getUpdatePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getDeletePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public Class<CouponResponse> getResponseClass() {
		return CouponResponse.class;
	}

	@Override
	public CouponRequest getRequest(Coupon object) {
		return new CouponRequest(object);
	}

	@Override
	public Coupon getResult(CouponResponse network) {
		return network.getCoupon();
	}

}
