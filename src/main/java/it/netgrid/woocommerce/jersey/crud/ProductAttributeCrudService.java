package it.netgrid.woocommerce.jersey.crud;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.ProductAttribute;
import it.netgrid.woocommerce.model.request.ProductAttributeRequest;
import it.netgrid.woocommerce.model.response.ProductAttributeResponse;

public class ProductAttributeCrudService extends TemplateCrudService<ProductAttribute, Integer, Object, ProductAttributeRequest, ProductAttributeResponse> {

	public static final String BASE_PATH = "products/attributes";
	public static final String BASE_PATH_FORMAT = "products/attributes/%d";
	
	public ProductAttributeCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Object context) {
		return BASE_PATH;
	}

	@Override
	public String getReadPath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getUpdatePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getDeletePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public Class<ProductAttributeResponse> getResponseClass() {
		return ProductAttributeResponse.class;
	}

	@Override
	public ProductAttributeRequest getRequest(ProductAttribute object) {
		return new ProductAttributeRequest(object);
	}

	@Override
	public ProductAttribute getResult(ProductAttributeResponse network) {
		return network.getProductAttribute();
	}

}
