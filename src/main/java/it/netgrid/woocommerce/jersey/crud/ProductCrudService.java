package it.netgrid.woocommerce.jersey.crud;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.Product;
import it.netgrid.woocommerce.model.request.ProductRequest;
import it.netgrid.woocommerce.model.response.ProductResponse;

public class ProductCrudService extends TemplateCrudService<Product, Integer, Object, ProductRequest, ProductResponse> {

	public static final String BASE_PATH = "products";
	public static final String BASE_PATH_FORMAT = "products/%d";
	
	public ProductCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Object context) {
		return BASE_PATH;
	}

	@Override
	public String getReadPath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getUpdatePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getDeletePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public Class<ProductResponse> getResponseClass() {
		return ProductResponse.class;
	}

	@Override
	public ProductRequest getRequest(Product object) {
		return new ProductRequest(object);
	}

	@Override
	public Product getResult(ProductResponse network) {
		return network.getProduct();
	}

}
