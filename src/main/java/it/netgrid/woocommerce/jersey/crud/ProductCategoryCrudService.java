package it.netgrid.woocommerce.jersey.crud;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.ProductCategory;
import it.netgrid.woocommerce.model.request.ProductCategoryRequest;
import it.netgrid.woocommerce.model.response.ProductCategoryResponse;

public class ProductCategoryCrudService extends TemplateCrudService<ProductCategory, Integer, Object, ProductCategoryRequest, ProductCategoryResponse> {

	public static final String BASE_PATH = "products/categories";
	public static final String BASE_PATH_FORMAT = "products/categories/%d";
	
	public ProductCategoryCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Object context) {
		return BASE_PATH;
	}

	@Override
	public String getReadPath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getUpdatePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getDeletePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public Class<ProductCategoryResponse> getResponseClass() {
		return ProductCategoryResponse.class;
	}

	@Override
	public ProductCategoryRequest getRequest(ProductCategory object) {
		return new ProductCategoryRequest(object);
	}

	@Override
	public ProductCategory getResult(ProductCategoryResponse network) {
		return network.getProductCategory();
	}

}
