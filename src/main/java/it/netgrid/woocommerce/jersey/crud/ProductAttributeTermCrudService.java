package it.netgrid.woocommerce.jersey.crud;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.ProductAttribute;
import it.netgrid.woocommerce.model.ProductAttributeTerm;
import it.netgrid.woocommerce.model.request.ProductAttributeTermRequest;
import it.netgrid.woocommerce.model.response.ProductAttributeTermResponse;

public class ProductAttributeTermCrudService extends TemplateCrudService<ProductAttributeTerm, Integer, ProductAttribute, ProductAttributeTermRequest, ProductAttributeTermResponse> {

	public static final String BASE_PATH = ProductAttributeCrudService.BASE_PATH_FORMAT + "/notes";
	public static final String BASE_PATH_FORMAT = ProductAttributeCrudService.BASE_PATH_FORMAT + "/notes/%d";
	
	public ProductAttributeTermCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(ProductAttribute context) {
		return String.format(BASE_PATH, context.getId());
	}

	@Override
	public String getReadPath(Integer id, ProductAttribute context) {
		return String.format(BASE_PATH_FORMAT, context.getId(), id);
	}

	@Override
	public String getUpdatePath(Integer id, ProductAttribute context) {
		return String.format(BASE_PATH_FORMAT, context.getId(), id);
	}

	@Override
	public String getDeletePath(Integer id, ProductAttribute context) {
		return String.format(BASE_PATH_FORMAT, context.getId(), id);
	}

	@Override
	public Class<ProductAttributeTermResponse> getResponseClass() {
		return ProductAttributeTermResponse.class;
	}

	@Override
	public ProductAttributeTermRequest getRequest(ProductAttributeTerm object) {
		return new ProductAttributeTermRequest(object);
	}

	@Override
	public ProductAttributeTerm getResult(ProductAttributeTermResponse network) {
		return network.getProductAttributeTerm();
	}

}
