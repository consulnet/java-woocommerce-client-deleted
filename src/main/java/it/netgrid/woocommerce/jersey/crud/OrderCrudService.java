package it.netgrid.woocommerce.jersey.crud;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.Order;
import it.netgrid.woocommerce.model.request.OrderRequest;
import it.netgrid.woocommerce.model.response.OrderResponse;

public class OrderCrudService extends TemplateCrudService<Order, Integer, Object, OrderRequest, OrderResponse>{

	public static final String BASE_PATH = "orders";
	public static final String BASE_PATH_FORMAT = "orders/%d";
	
	public OrderCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Object context) {
		return BASE_PATH;
	}

	@Override
	public String getReadPath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getUpdatePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getDeletePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public Class<OrderResponse> getResponseClass() {
		return OrderResponse.class;
	}

	@Override
	public OrderRequest getRequest(Order object) {
		return new OrderRequest(object);
	}

	@Override
	public Order getResult(OrderResponse network) {
		return network.getOrder();
	}

}
