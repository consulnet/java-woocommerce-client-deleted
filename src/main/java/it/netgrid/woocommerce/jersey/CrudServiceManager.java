package it.netgrid.woocommerce.jersey;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import it.netgrid.woocommerce.Configuration;
import it.netgrid.woocommerce.CrudService;
import it.netgrid.woocommerce.jersey.crud.CouponCrudService;
import it.netgrid.woocommerce.jersey.crud.CustomerCrudService;
import it.netgrid.woocommerce.jersey.crud.OrderCrudService;
import it.netgrid.woocommerce.jersey.crud.OrderNoteCrudService;
import it.netgrid.woocommerce.jersey.crud.OrderRefundCrudService;
import it.netgrid.woocommerce.jersey.crud.ProductAttributeCrudService;
import it.netgrid.woocommerce.jersey.crud.ProductAttributeTermCrudService;
import it.netgrid.woocommerce.jersey.crud.ProductCategoryCrudService;
import it.netgrid.woocommerce.jersey.crud.ProductCrudService;
import it.netgrid.woocommerce.jersey.crud.ProductShippingClassCrudService;
import it.netgrid.woocommerce.jersey.crud.ProductTagCrudService;
import it.netgrid.woocommerce.model.Coupon;
import it.netgrid.woocommerce.model.Customer;
import it.netgrid.woocommerce.model.Order;
import it.netgrid.woocommerce.model.OrderNote;
import it.netgrid.woocommerce.model.OrderRefund;
import it.netgrid.woocommerce.model.Product;
import it.netgrid.woocommerce.model.ProductAttribute;
import it.netgrid.woocommerce.model.ProductAttributeTerm;
import it.netgrid.woocommerce.model.ProductCategory;
import it.netgrid.woocommerce.model.ProductShippingClass;
import it.netgrid.woocommerce.model.ProductTag;

public class CrudServiceManager {
	
	public static final String V3 = "v3";
	public static final String URL_FORMAT = "%s/wc-api/%s%s";
	
	private static final Map<Configuration, WebTarget> targets = new HashMap<Configuration, WebTarget>();
	
	public static CrudService<Customer, Integer, Object> createCustomerService(Configuration config) {
		return new CustomerCrudService(CrudServiceManager.getWebTarget(config, V3));
	}
	
	public static CrudService<Coupon, Integer, Object> createCouponService(Configuration config) {
		return new CouponCrudService(CrudServiceManager.getWebTarget(config, V3));
	}
	
	public static CrudService<OrderNote, Integer, Order> createOrderNoteService(Configuration config) {
		return new OrderNoteCrudService(CrudServiceManager.getWebTarget(config, V3));
	}
	
	public static CrudService<OrderRefund, Integer, Order> createOrderRefundService(Configuration config) {
		return new OrderRefundCrudService(CrudServiceManager.getWebTarget(config, V3));
	}
	
	public static CrudService<Order, Integer, Object> createOrderService(Configuration config) {
		return new OrderCrudService(CrudServiceManager.getWebTarget(config, V3));
	}
	
	public static CrudService<ProductAttribute, Integer, Object> createProductAttribute(Configuration config) {
		return new ProductAttributeCrudService(CrudServiceManager.getWebTarget(config, V3));
	}
	
	public static CrudService<ProductAttributeTerm, Integer, ProductAttribute> createProductAttributeTermService(Configuration config) {
		return new ProductAttributeTermCrudService(CrudServiceManager.getWebTarget(config, V3));
	}
	
	public static CrudService<Product, Integer, Object> createProductService(Configuration config) {
		return new ProductCrudService(CrudServiceManager.getWebTarget(config, V3));
	}
	
	public static CrudService<ProductCategory, Integer, Object> createProductCategoryService(Configuration config) {
		return new ProductCategoryCrudService(CrudServiceManager.getWebTarget(config, V3));
	}
	
	public static CrudService<ProductShippingClass, Integer, Object> createProductShippingClassService(Configuration config) {
		return new ProductShippingClassCrudService(CrudServiceManager.getWebTarget(config, V3));
	}
	
	public static CrudService<ProductTag, Integer, Object> createProductTagService(Configuration config) {
		return new ProductTagCrudService(CrudServiceManager.getWebTarget(config, V3));
	}
	
	protected static WebTarget getWebTarget(Configuration config, String version) {
		WebTarget retval = targets.get(config);
		if(retval == null) {
			
			Client client = ClientBuilder.newClient();
			String targetPath = config.getBasePath();
			if(targetPath == null || targetPath.trim().equals("")) {
				targetPath = String.format(URL_FORMAT,config.getTargetUrl(), version,"");
			} else {
				targetPath = String.format(URL_FORMAT,config.getTargetUrl(), version, "/" + targetPath);
			}
			
			HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(config.getConsumerKey(), config.getConsumerSecret());
			client.register(feature);
			retval = client.target(targetPath);
			targets.put(config, retval);
		}
		
		return retval;
	}
}
