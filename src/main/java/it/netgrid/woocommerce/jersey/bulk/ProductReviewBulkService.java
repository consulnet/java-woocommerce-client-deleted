package it.netgrid.woocommerce.jersey.bulk;

import java.util.List;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.Product;
import it.netgrid.woocommerce.model.ProductReview;
import it.netgrid.woocommerce.model.response.ProductReviewsResponse;

public class ProductReviewBulkService extends TemplateReadOnlyBulkService<ProductReview, Integer, Product, ProductReviewsResponse> {

	public static final String READ_BASE_PATH = "products/%d/reviews";
	
	public ProductReviewBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(Product context) {
		return String.format(READ_BASE_PATH, context.getId());
	}

	@Override
	public Class<ProductReviewsResponse> getResponseClass() {
		return ProductReviewsResponse.class;
	}

	@Override
	public List<ProductReview> getResult(ProductReviewsResponse network) {
		return network.getProductReviews();
	}

}
