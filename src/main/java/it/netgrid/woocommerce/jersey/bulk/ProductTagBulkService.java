package it.netgrid.woocommerce.jersey.bulk;

import java.util.List;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.ProductTag;
import it.netgrid.woocommerce.model.response.ProductTagsResponse;

public class ProductTagBulkService extends TemplateReadOnlyBulkService<ProductTag, Integer, Object, ProductTagsResponse> {

	public static final String READ_BASE_PATH = "products/tags";
	
	public ProductTagBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(Object context) {
		return READ_BASE_PATH;
	}

	@Override
	public Class<ProductTagsResponse> getResponseClass() {
		return ProductTagsResponse.class;
	}

	@Override
	public List<ProductTag> getResult(ProductTagsResponse network) {
		return network.getProductTags();
	}

}
