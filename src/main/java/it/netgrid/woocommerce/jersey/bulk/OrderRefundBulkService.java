package it.netgrid.woocommerce.jersey.bulk;

import java.util.List;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.Order;
import it.netgrid.woocommerce.model.OrderRefund;
import it.netgrid.woocommerce.model.response.OrderRefundsResponse;

public class OrderRefundBulkService extends TemplateReadOnlyBulkService<OrderRefund, Integer, Order, OrderRefundsResponse> {

	public static final String READ_BASE_PATH = "orders/%d/refunds";
	
	public OrderRefundBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(Order context) {
		return String.format(READ_BASE_PATH, context.getId());
	}

	@Override
	public Class<OrderRefundsResponse> getResponseClass() {
		return OrderRefundsResponse.class;
	}

	@Override
	public List<OrderRefund> getResult(OrderRefundsResponse network) {
		return network.getOrderRefunds();
	}

}
