package it.netgrid.woocommerce.jersey.bulk;

import java.util.List;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.Order;
import it.netgrid.woocommerce.model.OrderNote;
import it.netgrid.woocommerce.model.response.OrderNotesResponse;

public class OrderNoteBulkService extends TemplateReadOnlyBulkService<OrderNote, Integer, Order, OrderNotesResponse> {

	public static final String READ_BASE_PATH = "orders/%d/notes";
	
	public OrderNoteBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(Order context) {
		return String.format(READ_BASE_PATH, context.getId());
	}

	@Override
	public Class<OrderNotesResponse> getResponseClass() {
		return OrderNotesResponse.class;
	}

	@Override
	public List<OrderNote> getResult(OrderNotesResponse network) {
		return network.getOrderNotes();
	}

}
