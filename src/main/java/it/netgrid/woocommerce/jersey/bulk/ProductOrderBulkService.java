package it.netgrid.woocommerce.jersey.bulk;

import java.util.List;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.Order;
import it.netgrid.woocommerce.model.Product;
import it.netgrid.woocommerce.model.response.OrdersResponse;

public class ProductOrderBulkService extends TemplateReadOnlyBulkService<Order, Integer, Product, OrdersResponse> {

	public static final String READ_BASE_PATH = "products/%d/orders";
	
	public ProductOrderBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(Product context) {
		return String.format(READ_BASE_PATH, context.getId());
	}

	@Override
	public Class<OrdersResponse> getResponseClass() {
		return OrdersResponse.class;
	}

	@Override
	public List<Order> getResult(OrdersResponse network) {
		return network.getOrders();
	}

}
