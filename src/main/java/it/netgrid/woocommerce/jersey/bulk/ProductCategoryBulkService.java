package it.netgrid.woocommerce.jersey.bulk;

import java.util.List;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.ProductCategory;
import it.netgrid.woocommerce.model.response.ProductCategoriesResponse;

public class ProductCategoryBulkService extends TemplateReadOnlyBulkService<ProductCategory, Integer, Object, ProductCategoriesResponse> {

	public static final String READ_BASE_PATH = "products/categories";
	
	public ProductCategoryBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(Object context) {
		return READ_BASE_PATH;
	}

	@Override
	public Class<ProductCategoriesResponse> getResponseClass() {
		return ProductCategoriesResponse.class;
	}

	@Override
	public List<ProductCategory> getResult(ProductCategoriesResponse network) {
		return network.getProductCategories();
	}

}
