package it.netgrid.woocommerce.jersey.bulk;

import java.util.List;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.ProductAttribute;
import it.netgrid.woocommerce.model.ProductAttributeTerm;
import it.netgrid.woocommerce.model.response.ProductAttributeTermsResponse;

public class ProductAttributeTermBulkService extends TemplateReadOnlyBulkService<ProductAttributeTerm, Integer, ProductAttribute, ProductAttributeTermsResponse> {

	public static final String READ_BASE_PATH = "products/attributes/%d/terms";
	
	public ProductAttributeTermBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(ProductAttribute context) {
		return String.format(READ_BASE_PATH, context.getId());
	}

	@Override
	public Class<ProductAttributeTermsResponse> getResponseClass() {
		return ProductAttributeTermsResponse.class;
	}

	@Override
	public List<ProductAttributeTerm> getResult(ProductAttributeTermsResponse network) {
		return network.getProductAttributeTerms();
	}

}
