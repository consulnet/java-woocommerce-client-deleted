package it.netgrid.woocommerce.jersey.bulk;

import java.util.List;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.ProductAttribute;
import it.netgrid.woocommerce.model.response.ProductAttributesResponse;

public class ProductAttributeBulkService extends TemplateReadOnlyBulkService<ProductAttribute, Integer, Object, ProductAttributesResponse> {

	public static final String READ_BASE_PATH = "products/attributes";
	
	public ProductAttributeBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(Object context) {
		return READ_BASE_PATH;
	}

	@Override
	public Class<ProductAttributesResponse> getResponseClass() {
		return ProductAttributesResponse.class;
	}

	@Override
	public List<ProductAttribute> getResult(ProductAttributesResponse network) {
		return network.getProductAttributes();
	}

}
