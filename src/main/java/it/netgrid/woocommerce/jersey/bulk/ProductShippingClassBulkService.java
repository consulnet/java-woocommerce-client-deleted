package it.netgrid.woocommerce.jersey.bulk;

import java.util.List;

import javax.ws.rs.client.WebTarget;

import it.netgrid.woocommerce.model.ProductShippingClass;
import it.netgrid.woocommerce.model.response.ProductShippingClassesResponse;

public class ProductShippingClassBulkService extends TemplateReadOnlyBulkService<ProductShippingClass, Integer, Object, ProductShippingClassesResponse>{

	public static final String READ_BASE_PATH = "products/shipping_classes";
	
	public ProductShippingClassBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(Object context) {
		return READ_BASE_PATH;
	}

	@Override
	public Class<ProductShippingClassesResponse> getResponseClass() {
		return ProductShippingClassesResponse.class;
	}

	@Override
	public List<ProductShippingClass> getResult(ProductShippingClassesResponse network) {
		return network.getProductShippingClasses();
	}

}
