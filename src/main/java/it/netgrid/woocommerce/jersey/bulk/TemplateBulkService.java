package it.netgrid.woocommerce.jersey.bulk;

import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;

import it.netgrid.woocommerce.CrudObject;
import it.netgrid.woocommerce.model.response.CountResponse;
import it.netgrid.woocommerce.BulkService;

public abstract class TemplateBulkService<T extends CrudObject<ID>, ID, C, R, S> implements BulkService<T, ID, C> {
	
	public static final String FILTER_QUERY_FORMAT = "filter[%s]";
	
	private final WebTarget target;
	
	protected TemplateBulkService(WebTarget target) {
		this.target = target;
	}

	@Override
	public List<T> write(List<T> items, C context) {
		Builder builder = this.initBuilder(this.getWritePath(context), null);
		S result = builder.post(this.getRequestEntity(items), this.getResponseClass());
		return this.getResult(result);
	}
	
	@Override
	public List<T> read(C context) {
		return this.read(context, null);
	}

	@Override
	public List<T> read(C context, Map<String, String> filter) {
		Builder builder = this.initBuilder(this.getReadPath(context), filter);
		S result = builder.get(this.getResponseClass());
		return this.getResult(result);
	}

	@Override
	public Integer count(C context, Map<String, String> filter) {
		Builder builder = this.initBuilder(this.getReadPath(context), filter);
		CountResponse result = builder.get(CountResponse.class);
		return result.getCount();
	}
	
	public Builder initBuilder(String path, Map<String, String> filter) {
		WebTarget target = this.target;
		if(path != null && !path.trim().equals("")) {
			target = target.path(path);
		}
		
		if(filter != null) {
			for(String param : filter.keySet()) {
				target = target.queryParam(String.format(FILTER_QUERY_FORMAT, param), filter.get(param));
			}
		}
		
		return target.request(MediaType.APPLICATION_JSON_TYPE);
	}
	
	public Entity<R> getRequestEntity(List<T> object) {
		R request = this.getWriteRequest(object);
		return Entity.entity(request,MediaType.APPLICATION_JSON_TYPE);
	}
	
	public abstract String getWritePath(C context);
	public abstract String getReadPath(C context);
	public abstract String getCountPath(C context);
	
	public abstract Class<S> getResponseClass();

	public abstract R getWriteRequest(List<T> items);
	public abstract List<T> getResult(S network);

}
