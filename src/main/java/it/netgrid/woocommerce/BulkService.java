package it.netgrid.woocommerce;

import java.util.List;
import java.util.Map;

public interface BulkService<T extends CrudObject<ID>, ID, C> {

	public List<T> write(List<T> items, C context);
	public List<T> read(C context);
	public List<T> read(C context, Map<String,String> filter);
	public Integer count(C context, Map<String,String> filter);
}
