package it.netgrid.woocommerce.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.CrudObject;

@XmlRootElement
public class ProductAttribute implements CrudObject<Integer> {

	public enum Type {
		@XmlEnumValue(value="select")
		SELECT,
		@XmlEnumValue(value="text")
		TEXT;
	}
	
	public enum OrderBy {
		@XmlEnumValue(value="menu_order")
		MENU_ORDER, 
		@XmlEnumValue(value="name")
		NAME, 
		@XmlEnumValue(value="name_num")
		NAME_NUM, 
		@XmlEnumValue(value="id")
		ID;
	}
	
	private Integer id;
	private String name;
	private String slug;
	private Type type;
	private OrderBy orderBy;
	private boolean hasArchives;
	
	public ProductAttribute() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	@XmlElement(name="order_by")
	public OrderBy getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(OrderBy orderBy) {
		this.orderBy = orderBy;
	}

	@XmlElement(name="has_archives")
	public boolean isHasArchives() {
		return hasArchives;
	}

	public void setHasArchives(boolean hasArchives) {
		this.hasArchives = hasArchives;
	}
	
	
}
