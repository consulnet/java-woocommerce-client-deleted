package it.netgrid.woocommerce.model.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.Product;

@XmlRootElement
public class ProductsResponse extends NetworkResponse {

	private List<Product> products;
	
	public ProductsResponse() {
		this.products = new ArrayList<Product>();
	}
	
	public ProductsResponse(List<Product> items) {
		this.products = items;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
}
