package it.netgrid.woocommerce.model.response;

import java.util.List;

import it.netgrid.woocommerce.model.Order;

public class OrdersResponse extends NetworkResponse {

	private List<Order> orders;
	
	public OrdersResponse() {}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

}
