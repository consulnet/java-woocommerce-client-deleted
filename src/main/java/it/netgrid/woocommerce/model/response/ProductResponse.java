package it.netgrid.woocommerce.model.response;

import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.Product;

@XmlRootElement
public class ProductResponse extends NetworkResponse {

	private Product product;
	
	public ProductResponse() {}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
}
