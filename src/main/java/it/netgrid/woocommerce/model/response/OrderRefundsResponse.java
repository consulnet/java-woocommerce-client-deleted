package it.netgrid.woocommerce.model.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.OrderRefund;

@XmlRootElement
public class OrderRefundsResponse extends NetworkResponse {

	private List<OrderRefund> orderRefunds;
	
	public OrderRefundsResponse() {
		this.orderRefunds = new ArrayList<OrderRefund>();
	}

	@XmlElement(name="order_refunds")
	public List<OrderRefund> getOrderRefunds() {
		return orderRefunds;
	}

	public void setOrderRefunds(List<OrderRefund> orderRefunds) {
		this.orderRefunds = orderRefunds;
	}
	
}
