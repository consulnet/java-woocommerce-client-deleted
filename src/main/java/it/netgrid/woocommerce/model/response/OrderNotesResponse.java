package it.netgrid.woocommerce.model.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.OrderNote;

@XmlRootElement
public class OrderNotesResponse extends NetworkResponse {

	private List<OrderNote> orderNotes;
	
	public OrderNotesResponse() {
		this.orderNotes = new ArrayList<OrderNote>();
	}

	@XmlElement(name="order_notes")
	public List<OrderNote> getOrderNotes() {
		return orderNotes;
	}

	public void setOrderNotes(List<OrderNote> orderNotes) {
		this.orderNotes = orderNotes;
	}
	
}
