package it.netgrid.woocommerce.model.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.ProductAttributeTerm;

@XmlRootElement
public class ProductAttributeTermsResponse extends NetworkResponse {

	private List<ProductAttributeTerm> productAttributeTerms; 
	
	public ProductAttributeTermsResponse() {
		this.productAttributeTerms = new ArrayList<ProductAttributeTerm>();
	}

	@XmlElement(name="product_attribute_terms")
	public List<ProductAttributeTerm> getProductAttributeTerms() {
		return productAttributeTerms;
	}

	public void setProductAttributeTerms(List<ProductAttributeTerm> productAttributeTerms) {
		this.productAttributeTerms = productAttributeTerms;
	}

}
