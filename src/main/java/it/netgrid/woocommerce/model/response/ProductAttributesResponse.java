package it.netgrid.woocommerce.model.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.ProductAttribute;

@XmlRootElement
public class ProductAttributesResponse extends NetworkResponse {

	private List<ProductAttribute> productAttributes;
	
	public ProductAttributesResponse() {
		this.productAttributes = new ArrayList<ProductAttribute>();
	}

	@XmlElement(name="product_attributes")
	public List<ProductAttribute> getProductAttributes() {
		return productAttributes;
	}

	public void setProductAttributes(List<ProductAttribute> productAttributes) {
		this.productAttributes = productAttributes;
	}
	
}
