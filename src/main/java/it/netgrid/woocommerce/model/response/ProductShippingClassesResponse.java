package it.netgrid.woocommerce.model.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.ProductShippingClass;

@XmlRootElement
public class ProductShippingClassesResponse extends NetworkResponse {

	private List<ProductShippingClass> productShippingClasses;
	
	public ProductShippingClassesResponse() {}
	
	public ProductShippingClassesResponse(List<ProductShippingClass> items) {
		this.productShippingClasses = items;
	}

	@XmlElement(name="product_shipping_classes")
	public List<ProductShippingClass> getProductShippingClasses() {
		return productShippingClasses;
	}

	public void setProductShippingClasses(List<ProductShippingClass> productShippingClasses) {
		this.productShippingClasses = productShippingClasses;
	}
	
}
