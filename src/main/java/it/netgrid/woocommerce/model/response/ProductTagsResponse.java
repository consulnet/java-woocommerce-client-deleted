package it.netgrid.woocommerce.model.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.ProductTag;

@XmlRootElement
public class ProductTagsResponse extends NetworkResponse {

	private List<ProductTag> productTags;
	
	public ProductTagsResponse() {
		this.productTags = new ArrayList<ProductTag>();
	}

	@XmlElement(name="product_tags")
	public List<ProductTag> getProductTags() {
		return productTags;
	}

	public void setProductTags(List<ProductTag> productTags) {
		this.productTags = productTags;
	}
	
}
