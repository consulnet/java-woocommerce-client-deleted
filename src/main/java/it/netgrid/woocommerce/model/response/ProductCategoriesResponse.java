package it.netgrid.woocommerce.model.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.ProductCategory;

@XmlRootElement
public class ProductCategoriesResponse extends NetworkResponse {

	private List<ProductCategory> productCategories;
	
	public ProductCategoriesResponse() {}
	
	public ProductCategoriesResponse(List<ProductCategory> items) {
		this.productCategories = items;
	}

	@XmlElement(name="product_categories")
	public List<ProductCategory> getProductCategories() {
		return productCategories;
	}

	public void setProductCategories(List<ProductCategory> productCategories) {
		this.productCategories = productCategories;
	}
	
}
