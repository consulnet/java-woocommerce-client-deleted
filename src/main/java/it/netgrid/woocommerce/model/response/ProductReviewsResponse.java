package it.netgrid.woocommerce.model.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.ProductReview;

@XmlRootElement
public class ProductReviewsResponse extends NetworkResponse {

	private List<ProductReview> productReviews;
	
	public ProductReviewsResponse() {
		this.productReviews = new ArrayList<ProductReview>();
	}

	@XmlElement(name="product_reviews")
	public List<ProductReview> getProductReviews() {
		return productReviews;
	}

	public void setProductReviews(List<ProductReview> productReviews) {
		this.productReviews = productReviews;
	}
	
}
