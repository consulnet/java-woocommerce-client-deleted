package it.netgrid.woocommerce.model.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.Coupon;

@XmlRootElement
public class CouponsResponse extends NetworkResponse {

	private List<Coupon> coupons;

	public CouponsResponse() {
		this.coupons = new ArrayList<Coupon>();
	}
	
	public List<Coupon> getCoupons() {
		return coupons;
	}

	public void setCoupons(List<Coupon> coupons) {
		this.coupons = coupons;
	}
}
