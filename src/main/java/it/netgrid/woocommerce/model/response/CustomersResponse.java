package it.netgrid.woocommerce.model.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.Customer;

@XmlRootElement
public class CustomersResponse extends NetworkResponse {

	private List<Customer> customers;

	public CustomersResponse() {
		this.customers = new ArrayList<Customer>();
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}
	
}
