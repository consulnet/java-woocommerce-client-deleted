package it.netgrid.woocommerce.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import it.netgrid.woocommerce.CrudObject;

@XmlRootElement
public class Product implements CrudObject<Integer> {
	
	public enum Type {
		@XmlEnumValue(value="simple")
		SIMPLE, 
		@XmlEnumValue(value="grouped")
		GROUPED, 
		@XmlEnumValue(value="external")
		EXTERNAL, 
		@XmlEnumValue(value="variable")
		VARIABLE;
	}
	
	public enum CatalogVisibility {
		@XmlEnumValue(value="visible")
		VISIBLE, 
		@XmlEnumValue(value="catalog")
		CATALOG, 
		@XmlEnumValue(value="search")
		SEARCH, 
		@XmlEnumValue(value="hidden")
		HIDDEN;
	}
	
	public enum Status {
		@XmlEnumValue(value="publish")
		PUBLISH;
	}
	
	public enum TaxStatus {
		@XmlEnumValue(value="taxable")
		TAXABLE, 
		@XmlEnumValue(value="shipping")
		SHIPPING, 
		@XmlEnumValue(value="none")
		NONE;
	}
	
	public enum DownloadType {
		@XmlEnumValue(value="")
		STANDARD, 
		@XmlEnumValue(value="application")
		APPLICATION, 
		@XmlEnumValue(value="music")
		MUSIC;
	}

	private String title;
	private Integer id;
	private String name;
	private Date createdAt;
	private Date updatedAt;
	private Type type;
	private Status status;
	private boolean downloadable;
	private boolean virtual;
	private String permalink;
	private String sku;
	private BigDecimal price;
	private BigDecimal regularPrice;
	private BigDecimal salePrice;
	private Date salePriceDatesFrom;
	private Date salePriceDatesTo;
	private String priceHtml;
	private boolean taxable;
	private TaxStatus taxStatus;
	private String taxClass;
	private boolean managingStock;
	private Integer stockQuantity;
	private boolean inStock;
	private boolean backordersAllowed;
	private boolean backordered;
	private boolean soldIndividually;
	private boolean purchaseable;
	private boolean featured;
	private boolean visible;
	private String catalogVisibility;
	private boolean onSale;
	private BigDecimal weight;
	private Dimension dimensions;
	private boolean shippingRequired;
	private boolean shippingTaxable;
	private boolean shippingClass;
	private Integer shippingClassId;
	private String description;
	private boolean enableHtmlDescription;
	private String shortDescription;
	private String enabledHtmlShortDescription;
	private boolean reviewsAllowed;
	private String averageRating;
	private Integer ratingCount;
	private List<Integer> relatedIds;
	private List<Integer> upsellIds;
	private List<Integer> crossSellIds;
	private Integer parentId;
	private List<String> categories;
	private List<String> tags;
	private List<Image> images;
	private List<Attribute> attributes;
	private List<DefaultAttribute> defaultAttributes;
	private List<Download> downloads;
	private Integer downloadLimit;
	private Integer downloadExpiry;
	private DownloadType downloadType; 
	private String purchaseNote;
	private Integer totalSales;
	private List<Variation> variations;
	private Product parent;
	private String productUrl;
	private String buttonText;
	private Integer menuOrder;
	
	public Product() {}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="created_at")
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@XmlElement(name="updated_at")
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public boolean isDownloadable() {
		return downloadable;
	}

	public void setDownloadable(boolean downloadable) {
		this.downloadable = downloadable;
	}

	public boolean isVirtual() {
		return virtual;
	}

	public void setVirtual(boolean virtual) {
		this.virtual = virtual;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@XmlElement(name="regular_price")
	public BigDecimal getRegularPrice() {
		return regularPrice;
	}

	public void setRegularPrice(BigDecimal regularPrice) {
		this.regularPrice = regularPrice;
	}

	@XmlElement(name="sale_price")
	public BigDecimal getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}

	@XmlElement(name="sale_price_dates_from")
	public Date getSalePriceDatesFrom() {
		return salePriceDatesFrom;
	}

	public void setSalePriceDatesFrom(Date salePriceDatesFrom) {
		this.salePriceDatesFrom = salePriceDatesFrom;
	}

	@XmlElement(name="sale_price_dates_to")
	public Date getSalePriceDatesTo() {
		return salePriceDatesTo;
	}

	public void setSalePriceDatesTo(Date salePriceDatesTo) {
		this.salePriceDatesTo = salePriceDatesTo;
	}

	@XmlElement(name="price_html")
	public String getPriceHtml() {
		return priceHtml;
	}

	public void setPriceHtml(String priceHtml) {
		this.priceHtml = priceHtml;
	}

	public boolean isTaxable() {
		return taxable;
	}

	public void setTaxable(boolean taxable) {
		this.taxable = taxable;
	}

	@XmlElement(name="tax_status")
	public TaxStatus getTaxStatus() {
		return taxStatus;
	}

	public void setTaxStatus(TaxStatus taxStatus) {
		this.taxStatus = taxStatus;
	}

	@XmlElement(name="tax_class")
	public String getTaxClass() {
		return taxClass;
	}

	public void setTaxClass(String taxClass) {
		this.taxClass = taxClass;
	}

	@XmlElement(name="managing_stock")
	public boolean isManagingStock() {
		return managingStock;
	}

	public void setManagingStock(boolean managingStock) {
		this.managingStock = managingStock;
	}

	@XmlElement(name="stock_quantity")
	public Integer getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(Integer stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	@XmlElement(name="in_stock")
	public boolean isInStock() {
		return inStock;
	}

	public void setInStock(boolean inStock) {
		this.inStock = inStock;
	}

	@XmlElement(name="backorders_allowed")
	public boolean isBackordersAllowed() {
		return backordersAllowed;
	}

	public void setBackordersAllowed(boolean backordersAllowed) {
		this.backordersAllowed = backordersAllowed;
	}

	public boolean isBackordered() {
		return backordered;
	}

	public void setBackordered(boolean backordered) {
		this.backordered = backordered;
	}

	@XmlElement(name="sold_individually")
	public boolean isSoldIndividually() {
		return soldIndividually;
	}

	public void setSoldIndividually(boolean soldIndividually) {
		this.soldIndividually = soldIndividually;
	}

	public boolean isPurchaseable() {
		return purchaseable;
	}

	public void setPurchaseable(boolean purchaseable) {
		this.purchaseable = purchaseable;
	}

	public boolean isFeatured() {
		return featured;
	}

	public void setFeatured(boolean featured) {
		this.featured = featured;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	@XmlElement(name="catalog_visibility")
	public String getCatalogVisibility() {
		return catalogVisibility;
	}

	public void setCatalogVisibility(String catalogVisibility) {
		this.catalogVisibility = catalogVisibility;
	}

	@XmlElement(name="on_sale")
	public boolean isOnSale() {
		return onSale;
	}

	public void setOnSale(boolean onSale) {
		this.onSale = onSale;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public Dimension getDimensions() {
		return dimensions;
	}

	public void setDimensions(Dimension dimensions) {
		this.dimensions = dimensions;
	}

	@XmlElement(name="shipping_required")
	public boolean isShippingRequired() {
		return shippingRequired;
	}

	public void setShippingRequired(boolean shippingRequired) {
		this.shippingRequired = shippingRequired;
	}

	@XmlElement(name="shipping_taxable")
	public boolean isShippingTaxable() {
		return shippingTaxable;
	}

	public void setShippingTaxable(boolean shippingTaxable) {
		this.shippingTaxable = shippingTaxable;
	}

	@XmlElement(name="shipping_class")
	public boolean isShippingClass() {
		return shippingClass;
	}

	public void setShippingClass(boolean shippingClass) {
		this.shippingClass = shippingClass;
	}

	@XmlElement(name="shipping_class_id")
	public Integer getShippingClassId() {
		return shippingClassId;
	}

	public void setShippingClassId(Integer shippingClassId) {
		this.shippingClassId = shippingClassId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name="enable_html_description")
	public boolean isEnableHtmlDescription() {
		return enableHtmlDescription;
	}

	public void setEnableHtmlDescription(boolean enableHtmlDescription) {
		this.enableHtmlDescription = enableHtmlDescription;
	}

	@XmlElement(name="short_description")
	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	@XmlElement(name="enabled_html_short_description")
	public String getEnabledHtmlShortDescription() {
		return enabledHtmlShortDescription;
	}

	public void setEnabledHtmlShortDescription(String enabledHtmlShortDescription) {
		this.enabledHtmlShortDescription = enabledHtmlShortDescription;
	}

	@XmlElement(name="reviews_allowed")
	public boolean isReviewsAllowed() {
		return reviewsAllowed;
	}

	public void setReviewsAllowed(boolean reviewsAllowed) {
		this.reviewsAllowed = reviewsAllowed;
	}

	@XmlElement(name="average_rating")
	public String getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(String averageRating) {
		this.averageRating = averageRating;
	}

	@XmlElement(name="rating_count")
	public Integer getRatingCount() {
		return ratingCount;
	}

	public void setRatingCount(Integer ratingCount) {
		this.ratingCount = ratingCount;
	}

	@XmlElement(name="related_ids")
	public List<Integer> getRelatedIds() {
		return relatedIds;
	}

	public void setRelatedIds(List<Integer> relatedIds) {
		this.relatedIds = relatedIds;
	}

	@XmlElement(name="upsell_ids")
	public List<Integer> getUpsellIds() {
		return upsellIds;
	}

	public void setUpsellIds(List<Integer> upsellIds) {
		this.upsellIds = upsellIds;
	}

	@XmlElement(name="cross_sell_ids")
	public List<Integer> getCrossSellIds() {
		return crossSellIds;
	}

	public void setCrossSellIds(List<Integer> crossSellIds) {
		this.crossSellIds = crossSellIds;
	}

	@XmlElement(name="parent_id")
	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}

	@XmlElement(name="default_attributes")
	public List<DefaultAttribute> getDefaultAttributes() {
		return defaultAttributes;
	}

	public void setDefaultAttributes(List<DefaultAttribute> defaultAttributes) {
		this.defaultAttributes = defaultAttributes;
	}

	public List<Download> getDownloads() {
		return downloads;
	}

	public void setDownloads(List<Download> downloads) {
		this.downloads = downloads;
	}

	@XmlElement(name="download_limit")
	public Integer getDownloadLimit() {
		return downloadLimit;
	}

	public void setDownloadLimit(Integer downloadLimit) {
		this.downloadLimit = downloadLimit;
	}

	@XmlElement(name="download_expiry")
	public Integer getDownloadExpiry() {
		return downloadExpiry;
	}

	public void setDownloadExpiry(Integer downloadExpiry) {
		this.downloadExpiry = downloadExpiry;
	}

	@XmlElement(name="download_type")
	public DownloadType getDownloadType() {
		return downloadType;
	}

	public void setDownloadType(DownloadType downloadType) {
		this.downloadType = downloadType;
	}

	@XmlElement(name="purchase_note")
	public String getPurchaseNote() {
		return purchaseNote;
	}

	public void setPurchaseNote(String purchaseNote) {
		this.purchaseNote = purchaseNote;
	}

	@XmlElement(name="total_sales")
	public Integer getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(Integer totalSales) {
		this.totalSales = totalSales;
	}

	public List<Variation> getVariations() {
		return variations;
	}

	public void setVariations(List<Variation> variations) {
		this.variations = variations;
	}

//	@XmlTransient
//	public Product getParent() {
//		return parent;
//	}
//
//	public void setParent(Product parent) {
//		this.parent = parent;
//	}

	@XmlElement(name="product_url")
	public String getProductUrl() {
		return productUrl;
	}

	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}

	@XmlElement(name="button_text")
	public String getButtonText() {
		return buttonText;
	}

	public void setButtonText(String buttonText) {
		this.buttonText = buttonText;
	}

	@XmlElement(name="menu_order")
	public Integer getMenuOrder() {
		return menuOrder;
	}

	public void setMenuOrder(Integer menuOrder) {
		this.menuOrder = menuOrder;
	}

	@XmlTransient
	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}
}
