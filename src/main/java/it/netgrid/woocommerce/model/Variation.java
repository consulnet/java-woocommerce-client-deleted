package it.netgrid.woocommerce.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.CrudObject;

@XmlRootElement
public class Variation implements CrudObject<Integer> {

	private Integer id;
	private Date createdAt;
	private Date updatedAt;
	private boolean downloadable;
	private boolean virtual;
	private String permalink;
	private String sku;
	private BigDecimal price;
	private BigDecimal regularPrice;
	private BigDecimal salePrice;
	private Date salePriceDatesFrom;
	private Date salePriceDatesTo;
	private boolean taxable;
	private Product.TaxStatus taxStatus;
	private String taxClass;
	private boolean managingStock;
	private Integer stockQuantity;
	private boolean inStock;
	private boolean backordered;
	private boolean purchaseable;
	private boolean visible;
	private boolean onSale;
	private BigDecimal weight;
	private Dimension dimensions;
	private String shippingClass;
	private Integer shippingClassId;
	private List<Image> image;
	private List<Attribute> attributes;
	private List<Download> downloads;
	private Integer downloadLimit;
	private Integer downloadExpiry;
	
	public Variation() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@XmlElement(name="created_at")
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@XmlElement(name="updated_at")
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public boolean isDownloadable() {
		return downloadable;
	}

	public void setDownloadable(boolean downloadable) {
		this.downloadable = downloadable;
	}

	public boolean isVirtual() {
		return virtual;
	}

	public void setVirtual(boolean virtual) {
		this.virtual = virtual;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@XmlElement(name="regular_price")
	public BigDecimal getRegularPrice() {
		return regularPrice;
	}

	public void setRegularPrice(BigDecimal regularPrice) {
		this.regularPrice = regularPrice;
	}

	@XmlElement(name="sale_price")
	public BigDecimal getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}

	@XmlElement(name="sale_price_dates_from")
	public Date getSalePriceDatesFrom() {
		return salePriceDatesFrom;
	}

	public void setSalePriceDatesFrom(Date salePriceDatesFrom) {
		this.salePriceDatesFrom = salePriceDatesFrom;
	}

	@XmlElement(name="sale_price_dates_to")
	public Date getSalePriceDatesTo() {
		return salePriceDatesTo;
	}

	public void setSalePriceDatesTo(Date salePriceDatesTo) {
		this.salePriceDatesTo = salePriceDatesTo;
	}

	public boolean isTaxable() {
		return taxable;
	}

	public void setTaxable(boolean taxable) {
		this.taxable = taxable;
	}

	@XmlElement(name="tax_status")
	public Product.TaxStatus getTaxStatus() {
		return taxStatus;
	}

	public void setTaxStatus(Product.TaxStatus taxStatus) {
		this.taxStatus = taxStatus;
	}

	@XmlElement(name="tax_class")
	public String getTaxClass() {
		return taxClass;
	}

	public void setTaxClass(String taxClass) {
		this.taxClass = taxClass;
	}

	@XmlElement(name="managing_stock")
	public boolean isManagingStock() {
		return managingStock;
	}

	public void setManagingStock(boolean managingStock) {
		this.managingStock = managingStock;
	}

	@XmlElement(name="stock_quantity")
	public Integer getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(Integer stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	@XmlElement(name="in_stock")
	public boolean isInStock() {
		return inStock;
	}

	public void setInStock(boolean inStock) {
		this.inStock = inStock;
	}

	public boolean isBackordered() {
		return backordered;
	}

	public void setBackordered(boolean backordered) {
		this.backordered = backordered;
	}

	public boolean isPurchaseable() {
		return purchaseable;
	}

	public void setPurchaseable(boolean purchaseable) {
		this.purchaseable = purchaseable;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	@XmlElement(name="on_sale")
	public boolean isOnSale() {
		return onSale;
	}

	public void setOnSale(boolean onSale) {
		this.onSale = onSale;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public Dimension getDimensions() {
		return dimensions;
	}

	public void setDimensions(Dimension dimensions) {
		this.dimensions = dimensions;
	}

	@XmlElement(name="shipping_class")
	public String getShippingClass() {
		return shippingClass;
	}

	public void setShippingClass(String shippingClass) {
		this.shippingClass = shippingClass;
	}

	@XmlElement(name="shippinh_class_id")
	public Integer getShippingClassId() {
		return shippingClassId;
	}

	public void setShippingClassId(Integer shippingClassId) {
		this.shippingClassId = shippingClassId;
	}

	public List<Image> getImage() {
		return image;
	}

	public void setImage(List<Image> image) {
		this.image = image;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}

	public List<Download> getDownloads() {
		return downloads;
	}

	public void setDownloads(List<Download> downloads) {
		this.downloads = downloads;
	}

	@XmlElement(name="download_limit")
	public Integer getDownloadLimit() {
		return downloadLimit;
	}

	public void setDownloadLimit(Integer downloadLimit) {
		this.downloadLimit = downloadLimit;
	}

	@XmlElement(name="download_expiry")
	public Integer getDownloadExpiry() {
		return downloadExpiry;
	}

	public void setDownloadExpiry(Integer downloadExpiry) {
		this.downloadExpiry = downloadExpiry;
	}
	
}
