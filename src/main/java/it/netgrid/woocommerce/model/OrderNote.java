package it.netgrid.woocommerce.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.CrudObject;

@XmlRootElement
public class OrderNote implements CrudObject<Integer> {

	private Integer id;
	private Date createdAt;
	private String note;
	private boolean customerNote;
	
	public OrderNote() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@XmlElement(name="created_at")
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@XmlElement(name="customer_note")
	public boolean isCustomerNote() {
		return customerNote;
	}

	public void setCustomerNote(boolean customerNote) {
		this.customerNote = customerNote;
	}
	
	
}
