package it.netgrid.woocommerce.model.request;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DeleteRequest {

	private boolean force;
	
	public DeleteRequest() {}

	public boolean isForce() {
		return force;
	}

	public void setForce(boolean force) {
		this.force = force;
	}
	
}
