package it.netgrid.woocommerce.model.request;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.Product;

@XmlRootElement
public class ProductBulkRequest {

	private List<Product> products;
	
	public ProductBulkRequest() {
		this.products = new ArrayList<Product>();
	}
	
	public ProductBulkRequest(List<Product> products) {
		this.products = products;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
}
