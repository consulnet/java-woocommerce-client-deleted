package it.netgrid.woocommerce.model.request;

import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.model.Product;

@XmlRootElement
public class ProductRequest {

	private Product product;
	
	public ProductRequest() {}
	
	public ProductRequest(Product product) {
		this.product = product;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
}
