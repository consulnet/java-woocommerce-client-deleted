package it.netgrid.woocommerce.model.request;

import java.util.ArrayList;
import java.util.List;

import it.netgrid.woocommerce.model.Order;

public class OrderBulkRequest {

	private List<Order> orders;
	
	public OrderBulkRequest() {
		this.orders = new ArrayList<Order>();
	}

	public OrderBulkRequest(List<Order> orders) {
		this.orders = orders;
	}
	
	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

}
