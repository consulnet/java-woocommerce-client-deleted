package it.netgrid.woocommerce.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.CrudObject;

@XmlRootElement
public class Image implements CrudObject<Integer> {

	private Integer id;
	private Date createdAt;
	private Date updatedAt;
	private String src;
	private String title;
	private String alt;
	private Integer posiiton;
	
	public Image() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@XmlElement(name="created_at")
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@XmlElement(name="updated_at")
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public Integer getPosiiton() {
		return posiiton;
	}

	public void setPosiiton(Integer posiiton) {
		this.posiiton = posiiton;
	}
	
	
}
