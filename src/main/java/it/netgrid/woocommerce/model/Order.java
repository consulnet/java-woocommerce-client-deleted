package it.netgrid.woocommerce.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.CrudObject;

@XmlRootElement
public class Order implements CrudObject<Integer> {
	
	@XmlRootElement
	public enum Status {
		@XmlEnumValue(value="pending")
		PENDING, 
		@XmlEnumValue(value="processing")
		PROCESSING, 
		@XmlEnumValue(value="on-hold")
		ON_HOLD, 
		@XmlEnumValue(value="completed")
		COMPLETED, 
		@XmlEnumValue(value="cancelled")
		CANCELLED, 
		@XmlEnumValue(value="refunded")
		REFUNDED, 
		@XmlEnumValue(value="failed")
		FAILED;
	}

	private Integer id;
	private String orderKey;
	private Integer orderNumber;
	private Date createdAt;
	private Date updatedAt;
	private Date completedAt;
	private Status status;
	private String currency;
	private BigDecimal total;
	private BigDecimal subtotal;
	private Integer totalLineItemsQuantity;
	private BigDecimal totalTax;
	private BigDecimal totalShipping;
	private BigDecimal cartTax;
	private BigDecimal shippingTax;
	private BigDecimal totalDiscount;
	private String shippingMethods;
	private PaymentDetails paymentDetails;
	private BillingAddress billingAddress;
	private ShippingAddress shippingAddress;
	private String note;
	private String customerIp;
	private String customerUserAgent;
	private Integer customerId;
	private String viewOrderUrl;
	private List<LineItem> lineItems;
	private List<ShippingLine> shippingLines;
	private List<TaxLine> taxLines;
	private List<FeeLine> feeLines;
	private List<CouponLine> couponLines;
	private Customer customer;
	
	public Order() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@XmlElement(name="order_number")
	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	@XmlElement(name="created_at")
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@XmlElement(name="updated_at")
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@XmlElement(name="completed_at")
	public Date getCompletedAt() {
		return completedAt;
	}

	public void setCompletedAt(Date completedAt) {
		this.completedAt = completedAt;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	@XmlElement(name="total_line_items_quantity")
	public Integer getTotalLineItemsQuantity() {
		return totalLineItemsQuantity;
	}

	public void setTotalLineItemsQuantity(Integer totalLineItemsQuantity) {
		this.totalLineItemsQuantity = totalLineItemsQuantity;
	}

	@XmlElement(name="total_tax")
	public BigDecimal getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(BigDecimal totalTax) {
		this.totalTax = totalTax;
	}

	@XmlElement(name="total_shipping")
	public BigDecimal getTotalShipping() {
		return totalShipping;
	}

	public void setTotalShipping(BigDecimal totalShipping) {
		this.totalShipping = totalShipping;
	}

	@XmlElement(name="cart_tax")
	public BigDecimal getCartTax() {
		return cartTax;
	}

	public void setCartTax(BigDecimal cartTax) {
		this.cartTax = cartTax;
	}

	@XmlElement(name="shipping_tax")
	public BigDecimal getShippingTax() {
		return shippingTax;
	}

	public void setShippingTax(BigDecimal shippingTax) {
		this.shippingTax = shippingTax;
	}

	@XmlElement(name="total_discount")
	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	@XmlElement(name="shipping_methods")
	public String getShippingMethods() {
		return shippingMethods;
	}

	public void setShippingMethods(String shippingMethods) {
		this.shippingMethods = shippingMethods;
	}

	@XmlElement(name="payment_details")
	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	@XmlElement(name="billing_address")
	public BillingAddress getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(BillingAddress billingAddress) {
		this.billingAddress = billingAddress;
	}

	@XmlElement(name="shipping_address")
	public ShippingAddress getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(ShippingAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@XmlElement(name="customer_ip")
	public String getCustomerIp() {
		return customerIp;
	}

	public void setCustomerIp(String customerIp) {
		this.customerIp = customerIp;
	}

	@XmlElement(name="customer_user_agent")
	public String getCustomerUserAgent() {
		return customerUserAgent;
	}

	public void setCustomerUserAgent(String customerUserAgent) {
		this.customerUserAgent = customerUserAgent;
	}

	@XmlElement(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	@XmlElement(name="view_order_url")
	public String getViewOrderUrl() {
		return viewOrderUrl;
	}

	public void setViewOrderUrl(String viewOrderUrl) {
		this.viewOrderUrl = viewOrderUrl;
	}

	@XmlElement(name="line_items")
	public List<LineItem> getLineItems() {
		return lineItems;
	}

	public void setLineItems(List<LineItem> lineItems) {
		this.lineItems = lineItems;
	}

	@XmlElement(name="shipping_lines")
	public List<ShippingLine> getShippingLines() {
		return shippingLines;
	}

	public void setShippingLines(List<ShippingLine> shippingLines) {
		this.shippingLines = shippingLines;
	}

	@XmlElement(name="tax_lines")
	public List<TaxLine> getTaxLines() {
		return taxLines;
	}

	public void setTaxLines(List<TaxLine> taxLines) {
		this.taxLines = taxLines;
	}

	@XmlElement(name="fee_lines")
	public List<FeeLine> getFeeLines() {
		return feeLines;
	}

	public void setFeeLines(List<FeeLine> feeLines) {
		this.feeLines = feeLines;
	}

	@XmlElement(name="coupon_lines")
	public List<CouponLine> getCouponLines() {
		return couponLines;
	}

	public void setCouponLines(List<CouponLine> couponLines) {
		this.couponLines = couponLines;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@XmlElement(name="order_key")
	public String getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}

}
