package it.netgrid.woocommerce.model;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

import it.netgrid.woocommerce.CrudObject;

@XmlRootElement
public class CouponLine implements CrudObject<Integer> {

	private Integer id;
	private String code;
	private BigDecimal amount;
	
	public CouponLine() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	
}
