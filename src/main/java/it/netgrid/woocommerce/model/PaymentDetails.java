package it.netgrid.woocommerce.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import it.netgrid.woocommerce.CrudObject;

@XmlRootElement
public class PaymentDetails implements CrudObject<String> {

	private String methodId;
	private String methodTitle;
	private boolean paid;
	private String transactionId;
	
	public PaymentDetails() {}

	@XmlElement(name="method_id")
	public String getMethodId() {
		return methodId;
	}

	public void setMethodId(String methodId) {
		this.methodId = methodId;
	}

	@XmlElement(name="method_title")
	public String getMethodTitle() {
		return methodTitle;
	}

	public void setMethodTitle(String methodTitle) {
		this.methodTitle = methodTitle;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	@XmlElement(name="transaction_id")
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	@XmlTransient
	public String getId() {
		return this.getMethodId();
	}
	
	
}
