package it.netgrid.woocommerce;

public interface CrudObject<ID> {

	public ID getId();
	
}